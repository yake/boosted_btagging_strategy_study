import ROOT
import time
import math

def get_significance(h_signal, h_background):
    bins = h_signal.GetNbinsX()
    Z = 0
    for bin in range(bins):
        si = h_signal.GetBinContent(bin+1) 
        bi = h_background.GetBinContent(bin+1)
        if bi > 1E-20 and si > 1E-20:
            Z += 2*((si+bi)*math.log(1+si/bi)-si)
    Z = math.sqrt(Z)
    return Z

f_tree_name = "./tag_r34-15/1L_tree.root"
f_tree = ROOT.TFile.Open(f_tree_name)
etree = f_tree.Get("Nominal")

sig_sample_list = ["qqWlvH125", "ggZllH125", "qqWlvH125", "qqZllH125", "qqZvvH125"]
bkg_sample_list = ["stops","stopt","stopWt","ttbar","Wcl","Wl","Wbc","Wbl","WZ","WW","ZZ","Zbl","Zcl","Zl","Zbc","Zbb","Zcc","ggZllH125cc","Wcc","ggZZ","Wbb","ggWW","qqWlvH125cc", "qqZllH125cc","ggZvvH125cc", "ttbar3pVR", "ttbar2VR", "qqZvvH125cc"]

sig_cut = "("
for sig_sample in sig_sample_list:
    sig_cut += "sample==\""+sig_sample+"\" || "
sig_cut = sig_cut[:-3] + ")"

bkg_cut = "("
for bkg_sample in bkg_sample_list:
    bkg_cut += "sample==\""+bkg_sample+"\" || "
bkg_cut = bkg_cut[:-3] + ")"

#ptv_list = ["pTV>250 && pTV<400", "pTV>400 && pTV<600", "pTV>600"]
ptv_list = ["pTV>250 && pTV<400", "pTV>400"]
addCaloJet_list = ["NAdditionalCaloJets==0","NAdditionalCaloJets>=1"]
#WPin_list = ["(bin_bTagBTrkJ1 == 5 && bin_bTagBTrkJ2 == 5)","((bin_bTagBTrkJ1 == 4 || bin_bTagBTrkJ2 == 4) && (bin_bTagBTrkJ1 != 3 && bin_bTagBTrkJ2 != 3))", "(bin_bTagBTrkJ1 == 3 || bin_bTagBTrkJ2 == 3)"] # WP: 0-60, 60-70, 70-77
#WPin_list = ["(bin_bTagBTrkJ1 >= 4 && bin_bTagBTrkJ2 >= 4)"] # WP: 0-70
#WPin_list = ["(bin_bTagBTrkJ1 == 5 && bin_bTagBTrkJ2 == 5)" , "((bin_bTagBTrkJ1 == 4 || bin_bTagBTrkJ2 == 4) && (bin_bTagBTrkJ1 != 3 && bin_bTagBTrkJ2 != 3))"] # WP: 0-60, 60-70
WPin_list = ["bin_bTagBTrkJ1 >= 4 && bin_bTagBTrkJ2 >= 4", "(bin_bTagBTrkJ1 == 3 || bin_bTagBTrkJ2 == 3)"] # WP: 0-70, 70-77
#WPin_list = ["bin_bTagBTrkJ1 >= 4 && bin_bTagBTrkJ2 >= 4", "((bin_bTagBTrkJ1 == 3 && bin_bTagBTrkJ2 >=4 )||(bin_bTagBTrkJ1 >= 4  && bin_bTagBTrkJ2 ==3 ))", "bin_bTagBTrkJ1 == 3 && bin_bTagBTrkJ2 == 3"] # First: 70WP, Second: pass 77WP but not 70WP, separating LT, TL with LL.

#WPout_list = ["bin_TrkJetNotInLeadFJMax<=1", "bin_TrkJetNotInLeadFJMax==2", "bin_TrkJetNotInLeadFJMax==3"] # WP: 100-85, 85-77, 77-70
WPout_list = ["bin_TrkJetNotInLeadFJMax<=1", "bin_TrkJetNotInLeadFJMax==2"] # WP: 100-85, 85-77
#WPout_list = ["bin_TrkJetNotInLeadFJMax==-99", "bin_TrkJetNotInLeadFJMax==1", "bin_TrkJetNotInLeadFJMax==2", "bin_TrkJetNotInLeadFJMax==3"] # WP: 100+, 100-85, 85-77, 77-70
#WPout_list = ["bin_TrkJetNotInLeadFJMax<=3"] # WP: 70-100
#WPout_list = ["bin_TrkJetNotInLeadFJMax<=1"] # WP: 85-100

region_idx = 0

for ptv in ptv_list:
    Z_ptv = 0
    S_ptv = 0
    for addCaloJet in addCaloJet_list:
        for WPin in WPin_list:
            for WPout in WPout_list:
                signal_name = "signal" + str(region_idx)
                background_name = "background" + str(region_idx)
                h_sig = ROOT.TH1F(signal_name, signal_name,10,0,200)
                h_bkg = ROOT.TH1F(background_name, background_name,10,0,200)
                cut = "EventWeight*(nFatJets>=1 &&"
                cut += ptv + " && "
                cut += addCaloJet + " && "
                cut += WPin + " && "
                cut += WPout + " && "
                cut_signal = cut + sig_cut + ")"
                cut_background = cut + bkg_cut + ")"
                etree.Draw("mJ>>"+signal_name,cut_signal,"goff")
                etree.Draw("mJ>>"+background_name,cut_background,"goff")
                Z = get_significance(h_sig, h_bkg)
                S = h_sig.Integral() # For debuging
                B = h_bkg.Integral() # For debuging
                
                #print(ptv+" "+addCaloJet+" "+WPin+" "+WPout+" :"+str(Z))
                # For debuging
                str_bkg = str(B)
                if B > 0:
                    #str_bkg = str(math.sqrt(B))
                    str_bkg = str(B) # stony brook adaptation, remove after use!!
                print(ptv+" "+addCaloJet+" "+WPin+" "+WPout+" : signal "+str(S)[:6] + " background "+str_bkg[:6] + " sigma: " + str(Z)[:6])
                region_idx += 1
                Z_ptv = math.sqrt(Z_ptv*Z_ptv + Z*Z)
                '''
                if WPin == "(bin_bTagBTrkJ1 == 3 || bin_bTagBTrkJ2 == 3)" and \
                        WPout == "bin_TrkJetNotInLeadFJMax==2" and \
                        ptv == "pTV>400" and \
                        addCaloJet == "NAdditionalCaloJets==0":
                        # WPin == "(bin_bTagBTrkJ1 == 5 && bin_bTagBTrkJ2 == 5)"
                    c = ROOT.TCanvas("c","c",750,850)
                    h_sig.SetLineColor(ROOT.kRed)
                    h_bkg.Draw("hist")
                    h_sig.Draw("hist SAME")
                    time.sleep(1000)
               '''  
    print(Z_ptv)
    #print(S_ptv) # for debuging
    print("")                
